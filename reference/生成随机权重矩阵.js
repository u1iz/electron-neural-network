const math = require('mathjs');
const rnorm = require('randgen').rnorm;
const randomNormal = require('random-normal');

function randomWeightMatrix(len1, len2) {
    let matrix = math.zeros(len1, len2);
    matrix = matrix.map(function(value, index, matrix) {
        // return rnorm(0, Math.pow(len1, -0.5));
        // return Math.randomNormal(0, Math.pow(len1, -0.5));
        return randomNormal(0, Math.pow(len1, -0.5));
    });
    return matrix;
}

console.log(randomWeightMatrix(5, 2))