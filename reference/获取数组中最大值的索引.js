const math = require('mathjs');

function npArgmax(arr) {
  const max = math.max(arr);
  return arr.indexOf(max);
}

const arr = [1, 3, 2, 5, 4];
const index = npArgmax(arr);
console.log(index); // 输出：3
