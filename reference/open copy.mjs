import open from 'open';
const http = require('http')


// console.log(111111);

// open(`http://localhost:3000/?output=${encodeURIComponent('Hello, World!')}`);


// node --experimental-modules  reference\open.mjs



const server = http.createServer((req, res) => {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello, World!');
  });
  
  server.listen(3000, () => {
    console.log('Server is running on http://localhost:3000');
  });