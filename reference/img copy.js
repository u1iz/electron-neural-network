const { createCanvas, loadImage } = require('canvas');
const fs = require('fs');
const path = require('path');

const imgArr = fs.readFileSync(path.join(__dirname, './img_data.json')).toJSON()

// 创建一个28x28的画布
const canvas = createCanvas(28, 28);
const ctx = canvas.getContext('2d');

// 将数组转换为图像数据
const imgData = ctx.createImageData(28, 28);

for (let i = 0; i < imgArr.length; i++) {
  const value = imgArr[i];
  imgData.data[i * 4] = value; // R
  imgData.data[i * 4 + 1] = value; // G
  imgData.data[i * 4 + 2] = value; // B
  imgData.data[i * 4 + 3] = 255; // Alpha
}

// 将图像数据绘制到画布上
ctx.putImageData(imgData, 0, 0);

// 将画布保存为PNG文件
const buffer = canvas.toBuffer('image/png');
fs.writeFileSync('image.png', buffer);
