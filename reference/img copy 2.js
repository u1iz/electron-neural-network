const { createCanvas, loadImage } = require('canvas');
const fs = require('fs');

// 创建画布
const canvas = createCanvas(400, 400);
const ctx = canvas.getContext('2d');

// 数组数据
const arr = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

// 渲染柱状图
const barWidth = 30;
const barSpacing = 10;
const maxBarHeight = 300;
const startX = 50;
const startY = 350;

ctx.fillStyle = 'blue';
for (let i = 0; i < arr.length; i++) {
  const barHeight = (arr[i] / Math.max(...arr)) * maxBarHeight;
  const x = startX + (barWidth + barSpacing) * i;
  const y = startY - barHeight;
  ctx.fillRect(x, y, barWidth, barHeight);
}

// 保存为PNG文件
const buffer = canvas.toBuffer('image/png');
fs.writeFileSync('output.png', buffer);
console.log('Image saved.');
