// self.lr * np.dot(hidden_error * self.hidden_output * (1 - self.hidden_output), np.transpose(input))
a = 0.3
d = [
    [1, 2, 3]
]
b = [[0.5], [-0.1]]
c = [[0.8], [0.2]]

function fn(a, b, c, d, e) {
    let t1 = math.dotMultiply(b, c)
    let t2 = math.subtract(1, c)


    let r1 = math.dotMultiply(t1, t2)
    let r2 = math.multiply(r1, d)
    let r3 = math.dotMultiply(a, r2)

    // console.log(t1);
    // console.log(t2);
    // console.log(r1);
    // console.log(r2);
    // console.log(r3);
    return math.add(e, r3)
}

e = [[-0.03, -0.12, -0.43], [0.97, 0.77, -0.42]]
console.log(fn(a,b,c,d,e));