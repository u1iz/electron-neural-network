const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', ws => {
  console.log('WebSocket连接已建立');

  // 发送输出到浏览器
  ws.send('Hello, World!');

  // 接收来自浏览器的消息
  ws.on('message', message => {
    console.log('接收到来自浏览器的消息:', message);
  });

  // 关闭WebSocket连接
  ws.on('close', () => {
    console.log('WebSocket连接已关闭');
  });
});


/*
const socket = new WebSocket('ws://localhost:8080');

socket.onopen = () => {
  console.log('WebSocket连接已建立');
};

socket.onmessage = event => {
  console.log('接收到来自服务器的消息:', event.data);
};

socket.onclose = () => {
  console.log('WebSocket连接已关闭');
};
*/